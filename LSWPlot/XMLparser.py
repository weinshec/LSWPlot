""" XMLparser.py - Input parser for convenient xml file format
"""

__author__ = 'Christoph Weinsheimer'
__email__  = 'weinshec@students.uni-mainz.de'


import LSWPlot.experiments
import logging
import numpy as np
import xml.etree.ElementTree


def parse(filename):
    """Create an experiment from an xml file

    Args:
        filename (str): filename incl. path of the xml file

    Returns:
        list: list of experiment instances
    """

    experiments = []

    try:
        tree = xml.etree.ElementTree.parse(filename)
        root = tree.getroot()
    except(FileNotFoundError):
        logging.error('Cannot parse file! No such file: %s' % filename)
        return []
    except(xml.etree.ElementTree.ParseError):
        logging.error('Cannot parse file! Not well-formed: %s' % filename)
        return []

    for node in root.getchildren():

        if node.tag == 'ALPsearch':
            e = LSWPlot.experiments.ALPsearch(node)
        elif node.tag == 'VMBsearch':
            e = LSWPlot.experiments.VMBsearch(node)
        else:
            raise RuntimeError('Unkown experiment type: %s' % node.tag)

        experiments.append(e)

    return experiments


def getAttribAsFloat(node, attribute):
    """Find and attribute of a given node and report an error if it is not
    valid.

    Args:
        node (Element): node in xml file
        attribute (str): name of the attribute to get

    Returns:
        np.float64: value of the attribute as double precision float

    Raises:
        AttributeError: if the attribute is not found
        ValueError: if the value is not castable to float
    """

    a = node.get(attribute)

    if a is None:
        raise AttributeError('Cannot find attribute    <%s>' % attribute)

    try:
        value = np.float64(a)
    except(ValueError):
        raise ValueError('Cannot parse value   %s    of attribute    <%s>'
            % (a.strip(), attribute))

    return value



def getChildAsFloat(node, attribute):
    """Find and attribute of a given node and report an error if it is not
    valid.

    Args:
        node (SubElement): experiment node in xml file
        attribute (str): name of the attribute to get

    Returns:
        np.float64: value of the attribute as double precision float

    Raises:
        AttributeError: if the attribute is not found
        ValueError: if the value is not castable to float
    """

    a = node.find(attribute)

    if a is None:
        raise AttributeError('Cannot find attribute    <%s>' % attribute)

    try:
        value = np.float64(a.text)
    except(ValueError):
        raise ValueError('Cannot parse value   %s    of attribute    <%s>'
            % (a.text.strip(), attribute))

    return value
