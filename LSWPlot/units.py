""" units.py - convenience functions to convert physical quantities to
Heavyside-Lorentz units
"""


__author__ = 'Christoph Weinsheimer'
__email__  = 'weinshec@students.uni-mainz.de'


import numpy as np
import scipy.constants as sci


__all__ = ['tesla_to_HLU',
           'meter_to_HLU',
           'joule_to_HLU',
           'wavelength_to_energy',
          ]


def tesla_to_HLU(B):
    """Convert magnetic field given in Tesla to eV^2

    Args:
        B (float): magnetic field in Tesla

    Returns:
        float: magnetic field in eV^2
    """

    return B * np.sqrt( sci.hbar**3 * sci.c**3 / sci.e**4 / sci.mu_0 )


def meter_to_HLU(l):
    """Convert a length given in meter to eV^-1

    Args:
        l (float): length given in meter

    Returns:
        float: length given in eV^-1
    """

    return l * sci.e / sci.c / sci.hbar


def joule_to_HLU(E):
    """Convert energy given in Joule to eV

    Args:
        E (float): energy given in Joule

    Returns:
        float: energy given in eV
    """

    return E / sci.e


def wavelength_to_energy(l):
    """Convert a photon wavelength to its according energy

    Args:
        l (float): photon wavelength given in meter

    Returns:
        float: photon energy given in eV
    """

    return sci.c * sci.h / l / sci.e
