""" VMBsearch.py - Defines the model of a set of parameters representing a
search for axion-like particles (ALPs) in VMB measurements.
"""

__author__ = 'Christoph Weinsheimer'
__email__  = 'weinshec@students.uni-mainz.de'


from collections import namedtuple
import logging
import LSWPlot.XMLparser
from LSWPlot.units import *
import numpy as np



class Magnet(namedtuple('Magnet', 'B L')):
    """Represents the configuration of a VMB magnet setup.
    """

    @classmethod
    def fromXML(cls, node):
        """Create a Magnet instance by parsing an XML node.

        The node has to feature the follwing the children providing the
        according parameters as their only attribute:
            * bfield  : [T] magnetic field
            * length  : [m] length of a (single) magnet

        Args:
            node (Element): xml tree Element feat. the parameters as children

        Returns:
            Magnet: instance of Magnet

        Raises:
            AttributeError: if one if the required parameters is not found or
                invalid
        """

        if node is None:
            raise AttributeError('Node <%s> not found!' % key)

        bfield  = LSWPlot.XMLparser.getChildAsFloat(node, 'bfield')
        length  = LSWPlot.XMLparser.getChildAsFloat(node, 'length')

        return cls(B   = tesla_to_HLU(bfield),
                   L   = meter_to_HLU(length))



class VMBsearch(object):
    """Represents a complete VMB search experiment configuration.
    """

    def __init__(self, xmlnode):
        """Constructor.

        The xmlnode has to feature child nodes named 'production',
        'regeneration' and 'detection' as well as the following attributes:
            * wavelength : [nm]   laser wavelength
            * n_u        : [T^-2] reduced induced birefringence

        Args:
            xmlnode (Element): xml tree Element feat. the parameters

        Raises:
            AttributeError: if one if the required parameters is not found or
                invalid
        """

        self.name       = xmlnode.get('name', 'unnamed')

        self.magnet = Magnet.fromXML(xmlnode.find('magnet'))

        wavel = LSWPlot.XMLparser.getAttribAsFloat(xmlnode, 'wavelength')
        n_u   = LSWPlot.XMLparser.getAttribAsFloat(xmlnode, 'n_u')
        self.E = wavelength_to_energy(wavel * 1e-9)
        self.n = n_u * self.magnet.B**2 / tesla_to_HLU(1)**2


    def g2(self, m):
        """Calculate the the squared coupling exclusion limit for a given mass.

        Args:
            m (float/ndarray-like) : ALP mass in eV

        Returns:
            float/ndarray-like : squared coupling constant exclusion limit
        """
        x = self.magnet.L * m**2 / (4.*self.E)
        return self.n * 2 * m**2 / self.magnet.B**2 \
            / (1. - np.sin(2.*x)/(2.*x))
