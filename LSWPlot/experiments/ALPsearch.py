""" ALPsearch.py - Defines the model of a set of parameters representing a
search for axion-like particles (ALPs)
experiment or exlcusion line.
"""

__author__ = 'Christoph Weinsheimer'
__email__  = 'weinshec@students.uni-mainz.de'


from collections import namedtuple
import logging
import LSWPlot.XMLparser
from LSWPlot.units import *
import numpy as np



class Cavity(namedtuple('Cavity', 'B L N gap PB n')):
    """Represents the configuration on either production or regeneration side.

    All production / regeneration configurations are considred as virtual
    optical cavities, even though no real resonator is installed. In this case
    just set the power-buildup (PB) to unity.
    """

    @classmethod
    def fromXML(cls, node):
        """Create a Cavity instance by parsing an XML node.

        The node has to feature the follwing the children providing the
        according parameters as their only attribute:
            * bfield  : [T] magnetic field
            * length  : [m] length of a (single) magnet
            * magnets : [ ] number of magnets
            * gap     : [m] field-free distance between the magnets
            * buildup : [ ] power buildup factor of the cavity (set 1 if none)
            * index   : [ ] index of refraction (e.g. for He applications)

        Args:
            node (Element): xml tree Element feat. the parameters as children

        Returns:
            Cavity: instance of Cavity

        Raises:
            AttributeError: if one if the required parameters is not found or
                invalid
        """

        if node is None:
            raise AttributeError('Node <%s> not found!' % key)

        bfield  = LSWPlot.XMLparser.getChildAsFloat(node, 'bfield')
        length  = LSWPlot.XMLparser.getChildAsFloat(node, 'length')
        magnets = LSWPlot.XMLparser.getChildAsFloat(node, 'magnets')
        gap     = LSWPlot.XMLparser.getChildAsFloat(node, 'gap')
        pb      = LSWPlot.XMLparser.getChildAsFloat(node, 'buildup')
        index   = LSWPlot.XMLparser.getChildAsFloat(node, 'index')

        return cls(B   = tesla_to_HLU(bfield),
                   L   = meter_to_HLU(length),
                   N   = magnets,
                   gap = meter_to_HLU(gap),
                   PB  = pb,
                   n   = index)



class Detection(namedtuple('Detection', 'eta rate')):
    """Represents the detection parameters of the experiment.
    """

    @classmethod
    def fromXML(cls, node):
        """Create a Detection instance by parsing an XML node.

        The node has to feature the follwing the children providing the
        according parameters as their only attribute:
            * efficiency  : [  ] detection efficiency
            * rate        : [Hz] excluded LSW flux

        Args:
            node (Element): xml tree Element feat. the parameters as children

        Returns:
            Detection: instance of Detection

        Raises:
            AttributeError: if one if the required parameters is not found or
                invalid
        """

        eff  = LSWPlot.XMLparser.getChildAsFloat(node, 'efficiency')
        rate = LSWPlot.XMLparser.getChildAsFloat(node, 'rate')

        return cls(eta=eff, rate=rate)



class ALPsearch(object):
    """Represents a complete ALP search experiment configuration.
    """

    def __init__(self, xmlnode):
        """Constructor.

        The xmlnode has to feature child nodes named 'production',
        'regeneration' and 'detection' as well as the following attributes:
            * power      : [W] incoming laser power
            * wavelength : [nm] laser wavelength

        Args:
            xmlnode (Element): xml tree Element feat. the parameters

        Raises:
            AttributeError: if one if the required parameters is not found or
                invalid
        """

        self.name       = xmlnode.get('name', 'unnamed')

        power = LSWPlot.XMLparser.getAttribAsFloat(xmlnode, 'power')
        wavel = LSWPlot.XMLparser.getAttribAsFloat(xmlnode, 'wavelength')
        self.P = joule_to_HLU(power)
        self.E = wavelength_to_energy(wavel * 1e-9)

        self.prod = Cavity.fromXML(xmlnode.find('production'))
        self.reg  = Cavity.fromXML(xmlnode.find('regeneration'))
        self.det  = Detection.fromXML(xmlnode.find('detection'))


    def _Pm(self, m, p):
        """Calculate the the modified conversion probability.

        The the modified conversion probability is defined as   P/g**2   with
        P being the ALPS-Photon conversion probability and g the coupling
        constant.

        Args:
            m (float, ndarray-like) : ALP mass in eV
            p (Cavity) : instance of cavity to calculate the probability for

        Return:
            float/ndarray-like : the conversion probability divided by g**2
        """

        M2 = m**2 + 2 * self.E**2 * (p.n - 1)
        L  = p.N * p.L
        return p.PB \
            * self.E / np.sqrt(self.E**2 - m**2) \
            * 4. * self.E**2 * p.B**2 / M2**2 \
            * np.sin(M2 * L / 4. / self.E / p.N)**2 \
            * np.sin(M2 * p.N * (L/p.N + p.gap) / 4. / self.E)**2 \
            / np.sin(M2       * (L/p.N + p.gap) / 4. / self.E)**2


    def g2(self, m):
        """Calculate the the squared coupling exclusion limit for a given mass.

        Args:
            m (float/ndarray-like) : ALP mass in eV

        Returns:
            float/ndarray-like : squared coupling constant exclusion limit
        """

        return np.sqrt(self.det.rate * self.E / self.P / self.det.eta) \
            / np.sqrt(self._Pm(m,self.prod) * self._Pm(m,self.reg))
