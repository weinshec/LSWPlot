""" plot.py - Class representing the exclusion plot
"""

__author__ = 'Christoph Weinsheimer'
__email__  = 'weinshec@students.uni-mainz.de'


import matplotlib.pyplot as plt
import numpy as np


class Plot(object):

    def __init__(self):
        self.fig, self.ax = plt.subplots()
        self.ax.set_xscale('log')
        self.ax.set_yscale('log')
        self.m = np.logspace(-5, -3, 1000)*5

        self.experiments = []


    def addExperiment(self, ex):
        g2 = ex.g2(self.m)
        self.ax.plot(self.m, np.sqrt(g2)/1e-9)
        self.ax.set_xlim(5e-5,5e-3)
        self.ax.set_ylim(1e-12,1e-6)
