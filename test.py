from LSWPlot import XMLparser, plot


ex = XMLparser.parse('experiments.xml')

p = plot.Plot()

for e in ex:
    p.addExperiment(e)
p.fig.savefig('text.pdf')
